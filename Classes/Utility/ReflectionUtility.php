<?php
namespace Comp\DevComponents\Utility;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 17/10/2016
 * Time: 14:23
 */
class ReflectionUtility implements \TYPO3\CMS\Core\SingletonInterface
{

    /**
     * @param $className
     * @return array
     */
    public static function getConstants($className) {
        $oClass = new \ReflectionClass($className);
        return $oClass->getConstants();
    }
}