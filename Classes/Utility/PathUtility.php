<?php
namespace Comp\DevComponents\Utility;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 17/10/2016
 * Time: 14:23
 */
class PathUtility implements \TYPO3\CMS\Core\SingletonInterface
{

    /**
     * normalize the file paths to a common path as an alternative to realpath
     * which only works with existing files
     * taken from: http://php.net/manual/de/function.realpath.php#112367
     *
     * @param $path
     * @return string
     */
    public static function normalizePath($path)
    {
        // Array to build a new path from the good parts
        $parts = array();
        // Replace backslashes with forwardslashes
        $path = str_replace('\\', '/', $path);
        // Combine multiple slashes into a single slash
        $path = preg_replace('/\/+/', '/', $path);
        // Collect path segments
        $segments = explode('/', $path);
        foreach ($segments as $segment) {
            if ($segment != '.') {
                $test = array_pop($parts);
                if (is_null($test)) {
                    $parts[] = $segment;
                } else if ($segment == '..') {
                    if ($test == '..') {
                        $parts[] = $test;
                    }
                    if ($test == '..' || $test == '') {
                        $parts[] = $segment;
                    }
                } else {
                    $parts[] = $test;
                    $parts[] = $segment;
                }
            }
        }
        return implode('/', $parts);
    }
}