<?php
namespace Comp\DevComponents\Component;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Ruben Pascal Abel <ruben.p.abel@gmail.com>,
 *           Steffen keuper <steffen.keuper@web.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ErrorAction Component
 */
trait ErrorActionComponent
{

    /**
     * A special action which is called if the originally intended action could
     * not be called, for example if the arguments were not valid.
     *
     * The default implementation sets a flash message, request errors and forwards back
     * to the originating action. This is suitable for most actions dealing with form input.
     *
     * We clear the page cache by default on an error as well, as we need to make sure the
     * data is re-evaluated when the user changes something.
     *
     * @return string
     * @todo Maybe we can also add Messages for Notices and Warnings...
     */
    protected function errorAction()
    {
        if (!isset($this->arguments) || !in_array("addFlashMessage", get_class_methods(__CLASS__))) {
            return parent::errorAction();
        }
        /** @var \TYPO3\CMS\Extbase\Validation\Error[] $errors */
        foreach ($this->arguments->getValidationResults()->getFlattenedErrors() as $path => $errors) {
            foreach ($errors as $error) {
                /** @noinspection PhpUndefinedMethodInspection */
                $this->addFlashMessage("An validation error occurred on {$path} - {$error->getMessage()} {$error->getCode()}", $error->getTitle(), \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR, false);
            }
        }
        return parent::errorAction();
    }

}