<?php
namespace Comp\DevComponents\Component;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Ruben Pascal Abel <ruben.p.abel@gmail.com>,
 *           Steffen keuper <steffen.keuper@web.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * CommandControllerRegister Component
 */
trait CommandControllerRegisterComponent {

	/**
	 * Registers this command controller into extbase
	 *
	 * @return void
	 */
	public static function register() {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = __CLASS__;
	}

	/**
	 * Unregisters this command controller from extbase
	 *
	 * @return void
	 */
	public static function unregister() {
		foreach( $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'] as $index => $class ) {
			if( $class == __CLASS__ ) {
				unset( $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][$index] );
				break;
			}
		}
	}

}