<?php
namespace Comp\DevComponents\Component;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Ruben Pascal Abel <ruben.p.abel@gmail.com>,
 *           Steffen keuper <steffen.keuper@web.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * OnAccessInjectorComponent
 *
 * Lets you annotate your classes with 'property' annotations to make properties 'lazy' loading!
 */
trait OnAccessInjectorComponent
{

    use AnnotatedClassPropertiesComponent {
        getAnnotatedClassProperties as private;
    }

    /**
     * Magic getter to resolve not loaded properties
     *
     * @param string $name Name of the property to get
     * @return \TYPO3\CMS\Extbase\Persistence\RepositoryInterface|object
     * @throws \Exception
     */
    public function __get($name)
    {
        $annotatedClassProperties = &$this->getAnnotatedClassProperties();
        if (isset($annotatedClassProperties[chr(36) . $name])) {
            return $this->$name = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager')->get($annotatedClassProperties[chr(36) . $name]);
        }
        try {
            if (substr($name, -10) == "Repository") {
                list($vendor, $ext, $path) = explode("\\", get_called_class(), 3);
                $repoName = "\\" . $vendor . "\\" . $ext . "\\Domain\\Repository\\" . ucfirst($name);
                // GeneralUtility::devLog("Repository name '$repoName' build from '$name'", GeneralUtility::camelCaseToLowerCaseUnderscored($ext));
                if (class_exists($repoName)) {
                    return $this->$name = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager')->get($repoName);
                }
                throw new \Exception("Repository " . $name . " is not valid or not creatable");
            }
            throw new \Exception($name . " is not a gettable Property/Object");
        } catch (\Exception $err) {
            GeneralUtility::devLog("Exception: " . $err->getMessage(), GeneralUtility::camelCaseToLowerCaseUnderscored($this->request->getControllerExtensionName()));
            throw $err;
        }
    }

}