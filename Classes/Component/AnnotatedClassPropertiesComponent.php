<?php
namespace Comp\DevComponents\Component;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Ruben Pascal Abel <ruben.p.abel@gmail.com>,
 *           Steffen keuper <steffen.keuper@web.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Reflection\ReflectionService;

/**
 * AnnotatedClassProperties Component
 */
trait AnnotatedClassPropertiesComponent
{

    /**
     * @var array
     */
    private $annotatedClassProperties;

    /**
     * Gets an array with all annotated class properties inside!
     *
     * @return array
     */
    protected function getAnnotatedClassProperties()
    {
        if (isset($this->annotatedClassProperties)) {
            return $this->annotatedClassProperties;
        }

        /** @var ReflectionService $reflectionService */
        $reflectionService = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Reflection\ReflectionService');
        $func = \Closure::bind(function ($className) {
            if (!isset($this->reflectedClassNames[$className])) {
                $this->reflectClass($className);
            }
            if (isset($this->classTagsValues[$className])) {
                return $this->classTagsValues[$className];
            }
        }, $reflectionService, get_class($reflectionService));
        $classTagsValues = $func(__CLASS__);
        if (isset($classTagsValues['property'])) {
            $properties = $classes = array();
            foreach ($classTagsValues['property'] as $oneTagValue) {
                list($classes[], $properties[], $description) = explode(" ", $oneTagValue, 3);
            }
            return $this->annotatedClassProperties = array_combine($properties, $classes);
        } else {
            return $this->annotatedClassProperties = array();
        }
    }

}