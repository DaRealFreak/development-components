<?php
namespace Comp\DevComponents\Resource;

use Comp\DevComponents\Domain\Model\FileReference;
use Comp\DevComponents\Utility\PathUtility;

/**
 * Created by PhpStorm.
 * User: User
 * Date: 17/10/2016
 * Time: 14:23
 */
class FileReferenceFactory
{

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper
     */
    protected $dataMapper;

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceStorage
     */
    protected $fileStorage;

    public function __construct()
    {
        if (!isset($this->objectManager)) {
            $this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        }

        if (!isset($this->dataMapper)) {
            $this->dataMapper = $this->objectManager->get('\TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper');
        }

        // initialize the file storage on "fileadmin" with the uid 1, only supporting fileadmin currently
        if (!isset($this->fileStorage)) {
            /** @var \TYPO3\CMS\Core\Resource\StorageRepository $storageRepository */
            $storageRepository = $this->objectManager->get('\TYPO3\CMS\Core\Resource\StorageRepository');
            $this->fileStorage = $storageRepository->findByUid(1);
        }
    }

    /**
     * Retrieves Extbase-model FileReference for given file path in local fileadmin
     *
     * @param string $filePath the path to the image
     * @param string $parentClassName class name of the parent object to have a workaround for the persistence bug
     * @param string $description the description of the image
     * @return NULL|FileReference
     */
    public function getFileReferenceFromPath($filePath, $parentClassName, $description = '')
    {

        if (!class_exists($parentClassName)) {
            return NULL;
        }

        /* if the class name is incorrect the data mapper can't retrieve any mappings
         * since persistence manager is working with get_class we imitate this process
         * to catch all possible class name mistakes with existing class names
         */
        $parentClassName = get_class($this->objectManager->get($parentClassName));
        $dataMap = $this->dataMapper->getDataMap($parentClassName);

        // case invalid classname or not traceable mapping
        if (!$tableName = $dataMap->getTableName()) {
            return NULL;
        }

        $filePath = PathUtility::normalizePath($filePath);
        // case no valid file
        if (!file_exists(PATH_site . 'fileadmin' . $filePath) || !is_file(PATH_site . 'fileadmin' . $filePath)) {
            return NULL;
        }

        /** @var \TYPO3\CMS\Core\Resource\File $fileObject */
        $fileObject = $this->fileStorage->getFile($filePath);

        if (is_object($fileObject)) {
            $resourceFileReferenceData = array(
                'uid_local' => $fileObject->getUid()
            );
            /** @var \TYPO3\CMS\Core\Resource\FileReference $resourceFileReference */
            $resourceFileReference = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Resource\FileReference', $resourceFileReferenceData);

            /** @var \Comp\DevComponents\Domain\Model\FileReference $fileReference */
            $fileReference = $this->objectManager->get('Comp\DevComponents\Domain\Model\FileReference');
            $fileReference->setOriginalResource($resourceFileReference);
            $fileReference->setLanguageUid($GLOBALS['TSFE']->sys_language_uid);
            $fileReference->setPid(intval($GLOBALS['TSFE']->id));
            // special properties not accessable normally
            $fileReference->setTablename($tableName);
            $fileReference->setDescription($description);
            return $fileReference;
        } else {
            return NULL;
        }
    }

}