<?php
namespace Comp\DevComponents\Examples;

/**
 * example for the command controller register component, you still have to call the register function of this class
 * in the ext_localconf.php to register it but won't have any problems with refactoring etc and looks cleaner
 */
class CacheCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController
{

    use \Comp\DevComponents\Component\CommandControllerRegisterComponent;

    /**
     * clear caches based on cmd, default is all
     *
     * @param string $cmd
     * @return void
     */
    public function clearCacheCommand($cmd)
    {
        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tce */
        $tce = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\DataHandling\\DataHandler');
        $tce->stripslashes_values = 0;
        $tce->admin = true;
        $tce->start(array(), array());
        $tce->clear_cacheCmd($cmd);
    }
}