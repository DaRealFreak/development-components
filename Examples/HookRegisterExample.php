<?php
namespace Comp\DevComponents\Examples;

/**
 * example for the hook register component, you still have to call the register function of this class
 * in the ext_localconf.php to register it, but you can define your hooks here and won't have any problems
 * with refactoring classes, namespaces, etc
 */
class HookRegisterExample
{
    use \Comp\DevComponents\Component\HookRegisterComponent;

    /*
     * you can define your hooks here, library -> hook -> function name
     */
    const associations = array(
        "tslib/class.tslib_fe.php" => array(
            "contentPostProc-all" => array(
                "postProcessContent"
            ),
            "contentPostProc-output" => array(
                "postProcessContent"
            )
        )
    );

    /**
     * replacing all mail addresses with a dummy mail
     *
     * @param $params
     * @param $obj
     */
    public function postProcessContent(&$params, &$obj)
    {
        $dummyMail = "dummy-mail@dumbdummy.com";
        $content = &$params['pObj']->content;
        $re = '/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i';
        preg_match_all($re, $content, $matches);
        foreach ($matches as $group) {
            foreach ($group as $match) {
                if ($match != $dummyMail) {
                    $content = str_replace($match, $dummyMail, $content);
                }
            }
        }
    }
}